from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
from .forms import QuestionForm, AnswerForm
from .models import Question, Tag, Answer
from django.contrib.auth.decorators import login_required
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from .serializers import QuestionSerializer,QuestionIdSerializer


@login_required(login_url='login')
def index(request):
    questions = Question.objects.all()
    return render(request, 'index.html', {'questions': questions})


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'register.html'


def add_tags(question, tags):
    for tag in tags:
        question_tag = Tag.objects.filter(tag=tag).first()
        if question_tag is None:
            question_tag = Tag(tag=tag)
            question_tag.save()
        question_tag.questions.add(question)


@login_required(login_url='login')
def new_question(request):
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            description = form.cleaned_data['question']
            question = Question(title=title, description=description)
            question.author = request.user
            question.save()
            tags = form.cleaned_data['tags'].split(',')
            add_tags(question, tags)
            # return redirect('question', pk=question.pk)
            return redirect('question_list')
        else:
            return render(request, 'new_question.html', {'form': form})
    else:
        form = QuestionForm()
        return render(request, 'new_question.html', {'form': form})


@login_required(login_url='login')
def question(request, pk):
    query = get_object_or_404(Question, pk=pk)
    if request.method == 'POST':
        form = AnswerForm(request.POST)
        if form.is_valid():
            answer = form.save(commit=False)
            answer.question = query
            answer.author = request.user
            answer.save()
            return redirect('question', pk=query.pk)

    else:
        form = AnswerForm()
        answers = Answer.objects.filter(question=pk)
    return render(request, 'question.html', {'question': query, 'answers': answers, 'form': form})


@login_required(login_url='login')
def tags(request):
    tags = Tag.objects.all()
    return render(request, 'tags.html', {'tags': tags})


@login_required(login_url='login')
def tag_questions(request, pk):
    tag = get_object_or_404(Tag, pk=pk)
    queries = tag.questions.all()
    return render(request, 'tags_question.html', {'questions': queries})


@api_view(['GET', 'POST','PUT'])
def question_list(request,format=None):
    if request.method == 'GET':
        serializer = QuestionSerializer(Question.objects.all(), many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = QuestionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data.update(author_id=request.user.pk)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def question_detail(request, pk,format=None):
    try:
        question = Question.objects.get(pk=pk)
    except Question.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = QuestionIdSerializer(question)
        return Response(serializer.data)
    elif request.method == 'PUT':
        serializer = QuestionIdSerializer(question, data=request.data)
        if serializer.is_valid():
            serializer.save(author_id=request.user.pk)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        question.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
