from rest_framework import serializers
from .models import Question, Answer,Tag


class AnswerSerializer(serializers.ModelSerializer):
    author=serializers.CharField(source='author.username',read_only=True)
    class Meta:
        model = Answer
        fields = ('id','answer','author')


class QuestionIdSerializer(serializers.ModelSerializer):
    answer=AnswerSerializer(source='answer_set',many=True,read_only=True)
    
    class Meta:
        model = Question
        fields = ('id', 'title', 'description','answer')

    # def create(self, validated_data):
    #     return Snippet.objects.create(**validated_data)

    # def update(self, instance, validated_data):
    #     instance.title = validated_data.get('title', instance.title)
    #     instance.code = validated_data.get('code', instance.code)
    #     instance.save()
    #     return instance
class QuestionSerializer(serializers.ModelSerializer):
    author=serializers.CharField(source='author.username',read_only=True)
    class Meta:
        model=Question
        fields=('id','title','description','author')
