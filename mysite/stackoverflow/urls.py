from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns=[path('',views.index,name='index'),
path('index',views.index,name='index'),
path('register',views.SignUp.as_view(),name='register'),
path('questions/new',views.new_question,name='new_question'),
path('questions/<int:pk>/',views.question,name='question'),
path('tags',views.tags,name='tags'),
path('tags/<int:pk>/',views.tag_questions,name='tag_questions'),
path('question_lists',views.question_list,name='question_list'),
path('question_details/<int:pk>/',views.question_detail,name='question_detail')
]
urlpatterns=format_suffix_patterns(urlpatterns)