from django import forms
from .models import Question,Answer

class QuestionForm(forms.Form):
    title=forms.CharField(max_length=200)
    question=forms.CharField(widget=forms.Textarea)
    tags=forms.CharField(max_length=300)


class AnswerForm(forms.ModelForm):
    answer=forms.CharField(label="",widget=forms.Textarea)
    class Meta:
        model=Answer
        fields=['answer']
        
